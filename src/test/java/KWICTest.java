import org.junit.jupiter.api.Test;
import java.util.ArrayList;
import static org.junit.jupiter.api.Assertions.*;

public class KWICTest {
    public ArrayList<String> set_input() {
        ArrayList<String>input=new ArrayList<>();
        input.add("is");
        input.add("the");
        input.add("of");
        input.add("and");
        input.add("as");
        input.add("a");
        input.add("but");
        input.add("::");
        input.add("Descent of Man");
        input.add("The Ascent of Man");
        input.add("The Old Man and The Sea");
        input.add("A Portrait of The Artist As a Young Man");
        input.add("A Man is a Man but Bubblesort IS A DOG");
        return input;
        }

    @Test
    void TestgetTitles(){
        ArrayList<String>input=set_input();
        ArrayList<String>expected=new ArrayList<>();
        expected.add("Descent of Man");
        expected.add("The Ascent of Man");
        expected.add("The Old Man and The Sea");
        expected.add("A Portrait of The Artist As a Young Man");
        expected.add("A Man is a Man but Bubblesort IS A DOG");

        assertEquals(KWIC.getTitles(input),expected);
    }

    @Test
    void TestgetIgnored(){
        ArrayList<String>input=set_input();
        ArrayList<String>expected=new ArrayList<>();
        expected.add("is");
        expected.add("the");
        expected.add("of");
        expected.add("and");
        expected.add("as");
        expected.add("a");
        expected.add("but");
        assertEquals(KWIC.getIgnored(input),expected);
    }

    @Test
    void TestgetKeyword(){
        ArrayList<String>input=set_input();
        ArrayList<String>expected=new ArrayList<>();
        expected.add("Artist");
        expected.add("Ascent");
        expected.add("Bubblesort");
        expected.add("Descent");
        expected.add("DOG");
        expected.add("Man");
        expected.add("Old");
        expected.add("Portrait");
        expected.add("Sea");
        expected.add("Young");
        assertEquals(KWIC.getKeyword(input),expected);
    }

    @Test
    void Testoutput(){
        ArrayList<String>input=set_input();
        ArrayList<String>expected=new ArrayList<>();
        expected.add("a portrait of the ARTIST as a young man");
        expected.add("the ASCENT of man");
        expected.add("a man is a man but BUBBLESORT is a dog");
        expected.add("DESCENT of man");
        expected.add("a man is a man but bubblesort is a DOG");
        expected.add("descent of MAN");
        expected.add("the ascent of MAN");
        expected.add("the old MAN and the sea");
        expected.add("a portrait of the artist as a young MAN");
        expected.add("a MAN is a man but bubblesort is a dog");
        expected.add("a man is a MAN but bubblesort is a dog");
        expected.add("the OLD man and the sea");
        expected.add("a PORTRAIT of the artist as a young man");
        expected.add("the old man and the SEA");
        expected.add("a portrait of the artist as a YOUNG man");
        assertEquals(KWIC.Output(input),expected);

    }






//    @Test
//    void TestTransfer() {
//        ArrayList<String>input=new ArrayList<>();
//        String input= "is\n"+ "the\n"+ "of\n"+ "and\n"+ "as\n"+ "a\n"+ "but\n"+"::\n"+
//                "Descent of Man\n"+ "The Ascent of Man\n" +"The Old Man and The Sea\n"+
//                "A Portrait of The Artist As a Young Man\n"+ "A Man is a Man but Bubblesort IS A DOG\n";
//
//        String output="a portrait of the ARTIST as a young man\n"+
//                "the ASCENT of man\n" +"a man is a man but BUBBLESORT is a dog\n"+
//                "DESCENT of man\n"+
//                "a man is a man but bubblesort is a DOG\n"+
//                "descent of MAN\n"+
//                "the ascent of MAN\n"+
//                "the old MAN and the sea\n"+
//                "a portrait of the artist as a young MAN\n"+
//                "a MAN is a man but bubblesort is a dog\n"+
//                "a man is a MAN but bubblesort is a dog\n"+
//                "the OLD man and the sea\n"+
//                "a PORTRAIT of the artist as a young man\n"+
//                "the old man and the SEA\n"+
//                "a portrait of the artist as a YOUNG man\n";
//
//        assertEquals(output.equals(KWIC.transfer(input)));
//
//
//
//
//    }

}