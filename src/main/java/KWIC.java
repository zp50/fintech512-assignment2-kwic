import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class KWIC {
    public static ArrayList<String> getTitles(ArrayList<String> input) {
        ArrayList<String> titles = new ArrayList<>();
        int split = 0;
        for (int i = 0; i < input.size(); i++) {

            if (input.get(i).equals("::")) {
                split = i;
            }
            if (i > split && split != 0) {
                titles.add(input.get(i));
            }

        }
        return titles;

    }

    public static ArrayList<String> getIgnored(ArrayList<String> input) {
        ArrayList<String> ignored = new ArrayList<>();
        for (int i = 0; i < input.size(); i++) {
            if (!input.get(i).equals("::")) {
                ignored.add(input.get(i));
            }
            if (input.get(i).equals("::")) {
                break;
            }
        }
        return ignored;

    }

    public static ArrayList<String> getKeyword(ArrayList<String> input) {
        ArrayList<String> keyword = new ArrayList<>();
        ArrayList<String> ignored = getIgnored(input);
        ArrayList<String> titles = getTitles(input);
        for (int i = 0; i < titles.size(); i++) {
            String title = titles.get(i);

            keyword.addAll(Arrays.asList(title.split(" ")));

            for (int j = 0; j < ignored.size(); j++) {
                for (int x = 0; x < keyword.size(); x++) {
                    if (keyword.get(x).equalsIgnoreCase(ignored.get(j))) {
                        keyword.remove(x);
                    }
                }
            }

        }

        //remove duplications and sort the keyword list in alphabetic order
        HashSet<String> set = new HashSet<>(keyword);
        keyword.clear();
        keyword.addAll(set);
        keyword.sort(String::compareToIgnoreCase);

        return keyword;
    }

    public static ArrayList<String> Output(ArrayList<String> input) {
        ArrayList<String> output = new ArrayList<>();
        ArrayList<String> titles = getTitles(input);
        ArrayList<String> keywords = getKeyword(input);

        for (int i = 0; i < keywords.size(); i++) {
            String keyword = keywords.get(i);
            for (int j = 0; j < titles.size(); j++) {
                String title = titles.get(j);
                if (title.contains(keyword)) {
                    int count = 0;
                    Pattern p = Pattern.compile(keyword);
                    Matcher m = p.matcher( title );
                    while (m.find()) {
                        count++;
                    }

                    int indx = title.indexOf(keyword);
                    ArrayList<Integer> indxs=new ArrayList<>();
                    indxs.add(indx);
                    while (indx >= 0) {
                        indx = title.indexOf(keyword, indx + 1);
                        indxs.add(indx);
                    }

                    for(int c=0;c<indxs.size()-1;c++){

                        if(indxs.get(c)!=0){
                            title = title.toLowerCase();
                            title = title.replace(keyword.toUpperCase(), keyword.toLowerCase());
                            title = title.substring(0,indxs.get(c))+keyword.toUpperCase()+title.substring(indxs.get(c)+keyword.length(),title.length());
                        }
                        else{
                            title = title.toLowerCase();
                            title = title.replaceFirst(keyword.toLowerCase(), keyword.toUpperCase());

                        }
                        output.add(title);

                    }

                }

            }

        }
        return output;
    }

    public static void main(String[] args) {

        ArrayList<String> input = new ArrayList<>();
        Scanner sc = new Scanner(System.in);
        String s = "";

        while(sc.hasNextLine()){
            s = sc.nextLine();
            if(s.equals("exit")){
                break;
            }
            input.add(s);
        }
        sc.close();
        ArrayList<String> output = Output(input);
        for (int i = 0; i < input.size(); i++) {
            System.out.println(output.get(i));
        }

    }


}

